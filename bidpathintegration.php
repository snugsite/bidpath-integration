<?php
   /*
   Plugin Name: SnugSite Bidpath Integration
   Description: Adds shortcodes for use with the Bid Path API. Available shortcodes are: [auctions], [upcoming], [eoi] and [apiCats]
   Version: 1
   Author: SnugSite
   Author URI: https://snugsite.com.au/
   License: GPL2
   */
?>

<?php

// enable errors
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);

// Not sure why we need this
function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}


// Auctions Shortcode
function auctions( $atts ){

  // View incoming XML
  //echo "<pre>"; print_r($bidPathXML); echo "</pre>";

	$randomString = generateRandomString();

	$bidPathXML = simplexml_load_file("http://auctions.dominiongroup.net.au/feed/dominion2.xml?$randomString&status=in_progress");	
  
	$noOfAuctions = count($bidPathXML['auctions']);

// Check to see if there are auctions
if(!empty($bidPathXML->auction)){


      // Echo out category if it's in the request
      if(isset($_GET['cat'])){
        echo '<h4>Category: '.$_GET['cat'].'</h4><br>';
      }

      // Remove non-category auctions if needed 
       if(isset($_GET['cat'])){
        $currentCat = $_GET['cat'];
        foreach ($bidPathXML->auction as $auction){
          if ($auction->category != $currentCat) {
            echo"Not in the current category";
          }
        }    
      } else {

	foreach ($bidPathXML->auction as $auction)
	{
      // Setup Auction attributes
      $title = '<a href="'.$auction->minfoURL.'">'.$auction->name.'</a>';
      $auctioneer = '<a href="'.$auction->minfoURL.'">'.$auction->auctioneer.'</a>';
      $auctionImage = '<img src="'.$auction->imageURL.'" alt="auction_image">';
      $startDate = $auction->startDate;
      $startDate = date("M d, Y", strtotime($startDate));
      $location = $auction->location;
      $description = $auction->shortDesc;
      $minfoURL = $auction->minfoURL;
      $mInfoButton = '<a href="'.$minfoURL.'"><button class="viewAuction">View Auction</button></a>';

      // Send information to the front end
      echo '
          <div class="vc_row wpb_row vc_row-fluid">
            <div class="wpb_column vc_column_container vc_col-sm-3">
              <div class="vc_column-inner ">
                <div class="wpb_wrapper"><div class="imageURL">'.$auctionImage.'</div>
                  
                  <div class="wpb_text_column wpb_content_element ">
                    <div class="wpb_wrapper"></div>
                    
                  </div>
                </div>
              </div>
            </div>
            <div class="wpb_column vc_column_container vc_col-sm-6">
              <div class="vc_column-inner ">
                <div class="wpb_wrapper"><div class="name">'.$title.'</div>
                  
                  <div class="wpb_text_column wpb_content_element">
                    <div class="wpb_wrapper"></div>
                    
                  </div>
                  <div class="wpb_text_column wpb_content_element ">
                    <div class="wpb_wrapper">
                      <div class="auctioneer">'.$auctioneer.'</div>
                      
                    </div>
                    
                  </div>
                  <div class="wpb_text_column wpb_content_element ">
                    <div class="wpb_wrapper">
                      <div class="description">'.$description.'</div>
                      
                    </div>
                    
                  </div>
                  <div class="wpb_text_column wpb_content_element ">
                    <div class="wpb_wrapper">
                      <div class="aDate">Auction Date</div><div class="startDate">'.$startDate.'</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="wpb_column vc_column_container vc_col-sm-3">
              <div class="vc_column-inner ">
                <div class="wpb_wrapper">
                  <div class="wpb_text_column wpb_content_element ">
                    <div class="wpb_wrapper">
                      <div class="location">'.$location.'</div>
                      
                    </div>
                    
                  </div>
                  <div class="vc_empty_space" style="height: 110px"><span class="vc_empty_space_inner"></span></div>
                  <div class="wpb_text_column wpb_content_element padding-fix">
                    <div class="wpb_wrapper">
                      <p>'.$mInfoButton.'</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
              <hr>
          </div><br>';
	}
	}
} else {
      echo '<div class="noauction">No Current Auctions</div>';
}

}
add_shortcode( 'auctions', 'auctions' );

/******************************** Start sam@snugsite.com.au code ********************************/


function xml2array ( $xmlObject, $out = array () )
{
    foreach ( (array) $xmlObject as $index => $node )
        $out[$index] = ( is_object ( $node ) ) ? xml2array ( $node ) : $node;
    return $out;
}

// Upcoming Auctions
function upcoming( $atts ){

  $randomString = generateRandomString();
  $xml = simplexml_load_file("http://auctions.dominiongroup.net.au/feed/dominion2.xml?$randomString&status=upcoming");

  $xml = xml2array($xml);

  // Count the number of auctions in the array
  if(!empty($xml['auction'])){
    $count = count($xml['auction']);

    if($count == 21){ // fixes BidPath not providing XML indexes if only 1 auction
      $count = count($xml);
    }

    $xml = json_decode( json_encode($xml) , 1); // json decode xml to array

    //Loop through array elements
    for($i=0;$i<$count;$i++){

      if($count < 2){
        $xmlArr[0] = $xml['auction']; //fix array offset issue when only 1 auction
      }else{
        $xmlArr[$i] = $xml['auction'][$i];
      }

      rtrim($xmlArr[$i]['name'], ">");

      $imageURL = '<img src="'. $xmlArr[$i]['imageURL'].'" alt="auction_image">';
      #$name = $xmlArr[$i]['name'];

      $name = '<a href="'.$xmlArr[$i]['minfoURL'].'">'.$xmlArr[$i]['name'].'</a>';


      $location = $xmlArr[$i]['location'];
      $auctioneer = $xmlArr[$i]['auctioneer'];
      $minfoURL = $xmlArr[$i]['minfoURL'];
      $minfoURL = '<a href="'.$minfoURL.'"><button class="viewAuction">View Auction</button></a>';
      $startDate = $xmlArr[$i]['startDate'];

      $startDate = date("M d, Y", strtotime($startDate));

      $descLoremIpsum = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vestibulum leo ante. Cras dolor ipsum, tincidunt id mattis egestas, pulvinar ultricies quam. Suspendisse vitae lectus ipsum. Etiam mauris tortor, suscipit non blandit sed, scelerisque eget lacus. Nam at tincidunt sem. Cras luctus tincidunt enim, sed auctor metus suscipit et. Integer quis ex erat.';

      #$description = $xmlArr[$i]['description'];

      $description = substr($descLoremIpsum,0,150).'...';




        echo '
        <div class="vc_row wpb_row vc_row-fluid">
          <div class="wpb_column vc_column_container vc_col-sm-3">
            <div class="vc_column-inner ">
              <div class="wpb_wrapper"><div class="imageURL">'.$imageURL.'</div>
                
                <div class="wpb_text_column wpb_content_element ">
                  <div class="wpb_wrapper"></div>
                  
                </div>
              </div>
            </div>
          </div>
          <div class="wpb_column vc_column_container vc_col-sm-6">
            <div class="vc_column-inner ">
              <div class="wpb_wrapper"><div class="name">'.$name.'</div>
                
                <div class="wpb_text_column wpb_content_element ">
                  <div class="wpb_wrapper"></div>
                  
                </div>
                <div class="wpb_text_column wpb_content_element ">
                  <div class="wpb_wrapper">
                    <div class="auctioneer">'.$auctioneer.'</div>
                    
                  </div>
                  
                </div>
                <div class="wpb_text_column wpb_content_element ">
                  <div class="wpb_wrapper">
                    <div class="description">'.$description.'</div>
                    
                  </div>
                  
                </div>
                <div class="wpb_text_column wpb_content_element ">
                  <div class="wpb_wrapper">
                    <div class="aDate">Auction Date</div><div class="startDate">'.$startDate.'</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="wpb_column vc_column_container vc_col-sm-3">
            <div class="vc_column-inner ">
              <div class="wpb_wrapper">
                <div class="wpb_text_column wpb_content_element ">
                  <div class="wpb_wrapper">
                    <div class="location">'.$location.'</div>
                    
                  </div>
                  
                </div>
                <div class="vc_empty_space" style="height: 110px"><span class="vc_empty_space_inner"></span></div>
                <div class="wpb_text_column wpb_content_element ">
                  <div class="wpb_wrapper">
                    <p>'.$minfoURL.'</p>
                    <hr>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>';


    }
  }else{
    echo '<div class="noauction">No Upcoming Auctions</div>';
  }
}
add_shortcode( 'upcoming', 'upcoming' );

// Expressions of Interest
function eoi( $atts ){
  $randomString = generateRandomString();
  $xml = simplexml_load_file("http://auctions.dominiongroup.net.au/feed/dominion2.xml?$randomString&status=in_progress");

  $xml = xml2array($xml);



  // Count the number of auctions in the array
  if(!empty($xml['auction'])){
    $count = count($xml['auction']);

    if($count == 21){ // fixes BidPath not providing XML indexes if only 1 auction
      $count = count($xml);
    }

    $xml = json_decode( json_encode($xml) , 1); // json decode xml to array



    //Remove non-EOIs from the array
    #for($i=0;$i<$count;$i++){

    #}


    //Loop through array elements
    for($i=0;$i<$count;$i++){



      if($count < 2){
        $xmlArr[0] = $xml['auction']; //fix array offset issue when only 1 auction
      }else{
        $xmlArr[$i] = $xml['auction'][$i];
      }

      #echo "<pre>";
      #print_r($xmlArr[$i]['eoi']);
      #echo "</pre>";

      if ($xmlArr[$i]['eoi'] == 'Yes') { // Check if auction or EOI (Yes = EOI)
        #$description = $dmsCruises[$i]['description'];

        rtrim($xmlArr[$i]['name'], ">");
        $eventId = $xmlArr[$i]['eventid'];

        $imageURL = '<img src="'. $xmlArr[$i]['imageURL'].'" alt="auction_image">';
        #$name = $xmlArr[$i]['name'];
        $name = '<a href="http://dominiongroup.snugprojects.com.au/expressions-of-interest/more-info/?eventid='.$eventId.'">'.$xmlArr[$i]['name'].'</a>';
        $location = $xmlArr[$i]['location'];
        $auctioneer = $xmlArr[$i]['auctioneer'];
        $minfoURL = $xmlArr[$i]['minfoURL'];




        #$minfoURL = '<a href="'.$minfoURL.'"><button class="viewAuction">Download PDF</button></a>';

        $eoiDesc = $xmlArr[$i]['eoiDesc'];

        if(empty($eoiDesc)){
          #echo "<h3>NO EOI DESC FOUND</h3>";
          $minfoURL = $xmlArr[$i]['eoiPDF'];
        }else{
          #$minfoURL = $eoiDesc;
          $minfoURL = '<a href="http://dominiongroup.snugprojects.com.au/expressions-of-interest/more-info/?eventid='.$eventId.'"><button class="viewAuction">More Info</button></a>';
        }



        $startDate = $xmlArr[$i]['startDate'];

        $startDate = date("M d, Y", strtotime($startDate));

        #$descLoremIpsum = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vestibulum leo ante. Cras dolor ipsum, tincidunt id mattis egestas, pulvinar ultricies quam. Suspendisse vitae lectus ipsum. Etiam mauris tortor, suscipit non blandit sed, scelerisque eget lacus. Nam at tincidunt sem. Cras luctus tincidunt enim, sed auctor metus suscipit et. Integer quis ex erat.';

        #$description = $xmlArr[$i]['description']; // Too long ... needs a blurb to be added by BidPath
        #$description = substr($descLoremIpsum,0,150).'...';

        echo '
            <div class="vc_row wpb_row vc_row-fluid">
              <div class="wpb_column vc_column_container vc_col-sm-3">
                <div class="vc_column-inner ">
                  <div class="wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element ">
                      <div class="wpb_wrapper">
                        <div class="imageURL">
                          '.$imageURL.'
                        </div>
                        
                      </div>
                      
                    </div>
                  </div>
                </div>
              </div>
              <div class="wpb_column vc_column_container vc_col-sm-3">
                <div class="vc_column-inner ">
                  <div class="wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element ">
                      <div class="wpb_wrapper">
                        <div class="name">
                          '.$name.'
                        </div>
                      </div>
                      
                    </div>
                  </div>
                </div>
              </div>
              <div class="wpb_column vc_column_container vc_col-sm-3">
                <div class="vc_column-inner ">
                  <div class="wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element ">
                      <div class="wpb_wrapper">
                        <div class="location">
                          '.$location.'
                        </div>
                        
                      </div>
                      
                    </div>
                  </div>
                </div>
              </div>
              <div class="wpb_column vc_column_container vc_col-sm-3">
                <div class="vc_column-inner ">
                  <div class="wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element ">
                      <div class="wpb_wrapper">
                        <div class="minfoURL">
                          '.$minfoURL.'
                        </div>
                        <hr>
                        
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div><br>
        ';
      }
    }
  }else{
    echo '<div class="noauction">No Current Expressions of Interest</div>';
  }
}
add_shortcode( 'eoi', 'eoi' );

// Expressions of Interest - More Info
function eoiMoreInfo( $atts ){

  $randomString = generateRandomString();
  $xml = simplexml_load_file("http://auctions.dominiongroup.net.au/feed/dominion2.xml?$randomString&status=in_progress");

  $xml = xml2array($xml);

  $eventId = $_GET['eventid'];

  #echo "<pre>";
  #print_r($xml);


  #echo $eventId;

  // Count the number of auctions in the array
  if(!empty($xml['auction'])){
    $count = count($xml['auction']);

    if($count == 21){ // fixes BidPath not providing XML indexes if only 1 auction
      $count = count($xml);
    }

    #echo $count;

    $xml = json_decode( json_encode($xml) , 1); // json decode xml to array

    // Unset all auctions which do not match the eventId GET var
    for($i=0;$i<$count;$i++){

      if($count < 2){
        $xmlArr[0] = $xml['auction']; //fix array offset issue when only 1 auction
      }else{
        $xmlArr[$i] = $xml['auction'][$i];
      }

      if($xmlArr[$i]['eventid'] != $eventId){
        unset($xmlArr[$i]);
        #echo '<h1>'.$xmlArr[$i]['eventid']."</h1><br>";
      }else{
        $xmlArrKey = $i;
      }
    }


    #$name = strip_tags($xmlArr[$xmlArrKey]['name']);
    $name = $xmlArr[$xmlArrKey]['name'];
    #$name = trim($name);

    $eoiDesc = $xmlArr[$xmlArrKey]['eoiDesc'];
    #$eoiDesc = strip_tags($xmlArr[$xmlArrKey]['eoiDesc']);
    #$eoiDesc = (string)$eoiDesc;

    #print_r($eoiDesc);

    #$eoiDesc = strip_tags($xmlArr[$xmlArrKey]['eoiDesc']);

    #$eoiDesc = json_encode($xmlArr[$xmlArrKey]['eoiDesc']);
    #$eoiDesc = strip_tags($eoiDesc);
    #$eoiDesc = trim($eoiDesc, '"'); // Strip the sky commas from the start of the description
    #$eoiDesc = rtrim($eoiDesc, '"'); // Strip the sky commas from the end of the description


    #$eoiDesc = strip_tags($eoiDesc);

    echo '
        <div class="vc_row wpb_row vc_row-fluid">
          <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner ">
              <div class="wpb_wrapper">
                <div class="wpb_text_column wpb_content_element">
                  <div class="wpb_wrapper">
                    <div class="eoiName">
                      <h3>'.$name.'</h3>
                    </div>
                  </div>
                  

                </div>
                <br><br>
                <div class="wpb_text_column wpb_content_element">
                  <div class="wpb_wrapper">
                    <div class="eoiDesc">
                      <p>'.$eoiDesc.'</p>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
        <br><br>
      ';



    #print_r($xmlArr);


  }

}
add_shortcode( 'eoiMoreInfo', 'eoiMoreInfo' );

// Get Categories List from the BidPath API
function apiCats( $atts ){
  $randomString = generateRandomString();
  $xml = simplexml_load_file("http://auctions.dominiongroup.net.au/feed/dominion2.xml?$randomString&status=in_progress");

  $xml = xml2array($xml);

  $xml = json_decode( json_encode($xml) , 1); // json decode xml to array

  #echo "<pre>";
  #print_r($xml);
  #echo "</pre>";

  if(!empty($xml['auction'])){
    $count = count($xml['auction']);

    //echo '<ul style="list-style: none !important;" class="leftNav">';
    #echo $count;

    for($i=0;$i<$count;$i++){
      $categories[$i] = $xml['auction'][$i]['category'];
    }

    $categories = array_unique($categories);

    $countCat = count($categories);

    for($j=0;$j<$countCat;$j++){
      echo '<img class="category-arrow" src="'.get_template_directory_uri().'/img/left-nav-arrow.png" /> <a class="category-link" href="/auctions/?cat='.$categories[$j].'">'.$categories[$j].'</a> </br>';
    }

    //echo '</ul>';
  }

  #print_r($categories);



}
add_shortcode( 'apiCats', 'apiCats' );


?>



